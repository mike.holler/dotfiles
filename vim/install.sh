#!/bin/bash

if [ ! -d "$HOME/.vim/bundle/Vundle.vim/.git" ]
then
    echo "--> Cloning Vundle..."
    git clone https://github.com/VundleVim/Vundle.vim.git "$HOME/.vim/bundle/Vundle.vim"
fi

echo "--> Updating Vundle..."
git -C "$HOME/.vim/bundle/Vundle.vim" pull

echo "--> Installing Vundle plugins..."
vim +PluginInstall +qall
