This is Vim Pencil
==================

Does Vim Pencil have hard or short wrapping on by default? Let's find out by typing a lot and seeing what happens. See the thing is, I don't really know, and this text is getting pretty wide. Will it wrap in the end, or does it stay on the same line? Let's find out by typing and typing and typing and seeing what happens. Does it wrap, or does it not? Who knows? Who cares? We'll see. Is the wrap soft or hard? It appears to be soft.

This is with soft pencil enabled, if I type and type and type, wrapping should happen naturally, and it should break over a line. I'm not sure if I like this or not, but we'll see when I continue typing. Is this a full line break? No...

This is vim with :HardPencil enabled. If I type and type and type, this is
really the best of all  worlds, here. Wow. I like this a lot. Should
effortlessly reflow when I type them.
