#!/bin/bash

echo "In python/install.sh"

if [ ! -d "$HOME/.miniconda" ]
then
    echo "--> Installing miniconda..."
    wget http://repo.continuum.io/miniconda/Miniconda3-3.7.0-Linux-x86_64.sh -O /tmp/miniconda.sh
    echo "--> Installing miniconda..."
    bash /tmp/miniconda.sh -b -p "$HOME/.miniconda"
fi

if [ ! -d "$HOME/.pyenv" ]
then
    echo "--> Installing pyenv..."
    git clone https://github.com/pyenv/pyenv.git ~/.pyenv
fi

if [ -d "$HOME/.pyenv" ]
then
    echo "--> Updating pyenv..."
    cd ~/.pyenv
    git pull
fi

if [ ! -d "$HOME/.pyenv/plugins/pyenv-virtualenv" ]
then
    git clone https://github.com/pyenv/pyenv-virtualenv.git "$HOME/.pyenv/plugins/pyenv-virtualenv"
fi

