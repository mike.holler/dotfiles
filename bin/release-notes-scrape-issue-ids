#!/usr/bin/env bash

set -eo pipefail

print_help() {
    echo "Usage: release-notes-scrape-issue-ids <PROJECT> <FROM_REF> <TO_REF>"
}

if [ "$#" -ne 3 ]; then
    print_help
    exit 1
fi

PROJECT=$1   # JIRA Project ID like CLOUD or ANDR
FROM_REF=$2  # Git ref from the older comparison reference
TO_REF=$3    # Git ref from the newer comparison reference

# This pattern finds all instances of project identifiers. It also supports the
# special format we use in branch names when we have multiple JIRA tickets.
# Here are a few inputs that will match this pattern (assuming PROJECT="CLOUD"):
#
#     CLOUD-1
#     CLOUD-1234
#     CLOUD-1234-5678
#
LOG_PATTERN="${PROJECT}-[0-9]+(-[0-9]+)*"

issue_ids=$(
    git log "${FROM_REF}..${TO_REF}" |  # Retrive logs for desired range
    egrep -o "${LOG_PATTERN}" |         # Output each JIRA identifiers string
    egrep -o "[0-9]+" |                 # Output each ticket number
    awk "{print \"${PROJECT}-\" \$0}" |    # Re-add project prefix to each number
    sort | uniq |                       # Keep only distinct lines
    paste -sd ',' -                     # Join lines with comma
)

# Prints JIRA query.
echo "id in ($issue_ids)"

