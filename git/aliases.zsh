# The rest of my fun git aliases
# Example:
# alias gl='git pull --prune'

# Go to root of current git repository.
alias gitroot='cd "$(git rev-parse --show-cdup)"'

