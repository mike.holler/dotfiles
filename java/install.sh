#!/bin/bash

if [ ! -d "$HOME/.sdkman" ]
then
    echo "--> Installing sdkman..."
    curl -s "https://get.sdkman.io" | bash
fi

source "$HOME/.sdkman/bin/sdkman-init.sh"

echo "--> Updating sdkman..."
sdk selfupdate

sdk install java
sdk install gradle
sdk install kotlin
sdk install maven
sdk install kscript
sdk update
