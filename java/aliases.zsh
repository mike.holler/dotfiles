# Loads the sdk command. Not technically an alias, but close enough.
[[ -s "$HOME/.sdkman/bin/sdkman-init.sh" ]] && source "$HOME/.sdkman/bin/sdkman-init.sh"

alias use-java-8='sdk use java $(sdk list java | egrep -m 1 -o "(8\.[0-9]+\.[0-9]+-zulu[^a-z])")'
alias use-java-11='sdk use java $(sdk list java | egrep -m 1 -o "(11\.[0-9]+\.[0-9]+-zulu[^a-z])")'
